package app;

import model.Network;
import org.apache.log4j.Logger;
public class App {
    private static final Logger logger = Logger.getLogger(App.class);

    public static void main(String[] args) {
        Wine wineTest = new Wine();
        Network<Integer>.Results results = wineTest.classify();
        logger.info(results.correct + " correct of " + results.trials + " = " + results.percentage * 100 + "%");
    }
}
