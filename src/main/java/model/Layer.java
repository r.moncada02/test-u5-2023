
package model;

import util.Util;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.DoubleUnaryOperator;

public class Layer {
	private Optional<Layer> previousLayer;
	private List<Neuron> neurons = new ArrayList<>();
	private double[] outputCache;

	public Layer(Optional<Layer> previousLayer, int numNeurons, double learningRate,
				 DoubleUnaryOperator activationFunction, DoubleUnaryOperator derivativeActivationFunction) {
		this.previousLayer = previousLayer;
		for (int i = 0; i < numNeurons; i++) {
			double[] randomWeights = null;
			if (previousLayer.isPresent()) {
				SecureRandom random = new SecureRandom();
				byte[] randomBytes = new byte[previousLayer.get().neurons.size()];
				random.nextBytes(randomBytes);
				randomWeights = new double[randomBytes.length];
				for (int j = 0; j < randomBytes.length; j++) {
					randomWeights[j] = randomBytes[j];
				}
			}
			Neuron neuron = new Neuron(randomWeights, learningRate, activationFunction, derivativeActivationFunction);
			neurons.add(neuron);
		}
		outputCache = new double[numNeurons];
	}

	public double[] outputs(double[] inputs) {
		if (previousLayer.isPresent()) {
			outputCache = neurons.stream().mapToDouble(n -> n.output(inputs)).toArray();
		} else {
			outputCache = inputs;
		}
		return outputCache;
	}

	/**
	 * should only be called on output layer
	 * @param expected
	 */
	public void calculateDeltasForOutputLayer(double[] expected) {
		for (int n = 0; n < neurons.size(); n++) {
			neurons.get(n).setDelta(neurons.get(n).derivativeActivationFunction.applyAsDouble(neurons.get(n).getOutputCache())
					* (expected[n] - outputCache[n]));
		}
	}

	/**
	 * should not be called on output layer
	 * @param nextLayer
	 */
	public void calculateDeltasForHiddenLayer(Layer nextLayer) {
		for (int i = 0; i < neurons.size(); i++) {
			int index = i;
			double[] nextWeights = nextLayer.neurons.stream().mapToDouble(n -> n.getWeights()[index]).toArray();
			double[] nextDeltas = nextLayer.neurons.stream().mapToDouble(Neuron::getDelta).toArray();
			double sumWeightsAndDeltas = Util.dotProduct(nextWeights, nextDeltas);
			neurons.get(i).setDelta(neurons.get(i).derivativeActivationFunction
					.applyAsDouble(neurons.get(i).getOutputCache()) * sumWeightsAndDeltas);
		}
	}

	public Optional<Layer> getPreviousLayer() {
		return previousLayer;
	}

	public List<Neuron> getNeurons() {
		return neurons;
	}

	public double[] getOutputCache() {
		return outputCache;
	}

	public void setOutputCache(double[] outputCache) {
		this.outputCache = outputCache;
	}
}
